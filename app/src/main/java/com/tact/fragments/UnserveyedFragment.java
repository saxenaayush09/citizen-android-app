package com.tact.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tact.database.MyDbHelper;
import com.tact.kumbhca.R;
import com.tact.model.GetVotersRequestModel;
import com.tact.model.GetVotersResponseModel;
import com.tact.model.VoterData;
import com.tact.network.ApiClient;
import com.tact.network.ApiClientInterface;
import com.trend.progress.ProgressDialog;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UnserveyedFragment extends Fragment {
    View view;
    private Context context;
    private ApiClientInterface apiClientInterface;
    private GetVotersResponseModel getVotersResponseModel;
    private ArrayList<VoterData> voterList = new ArrayList<>();
    private RecyclerView recyclerview_all;
    ProgressDialog progressBar;
    private MyDbHelper myDbHelper;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_unserveyed, container, false);
        initWidgets();
        initWidgetListener();

        callGetVotersData(getArguments().getInt("PsNo"), getArguments().getInt("AcNo"), getArguments().getString("Prefix"));

        return view;
    }

    private void initWidgetListener() {


    }

    private void initWidgets() {
        context = getActivity();
        apiClientInterface = ApiClient.getApiClient().create(ApiClientInterface.class);
        recyclerview_all = view.findViewById(R.id.recyclerview_all);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerview_all.setLayoutManager(linearLayoutManager);
        progressBar = new ProgressDialog(context);
        progressBar.setBarColor(getResources().getColor(R.color.red));
        myDbHelper = new MyDbHelper(context);

    }


    private void callGetVotersData(int psNo, int acNo, String prefix) {
        progressBar.show();

        GetVotersRequestModel item = new GetVotersRequestModel();
        item.setAcNo(acNo);
        item.setPsNo(psNo);
        item.setPrefix(prefix);

        //final GetVotersRequestModel getVotersRequestModel = createGetVotersRequestModel();

        apiClientInterface.getVoters(item).enqueue(new Callback<GetVotersResponseModel>() {
            @Override
            public void onResponse(Call<GetVotersResponseModel> call, Response<GetVotersResponseModel> response) {
                progressBar.dismiss();

                getVotersResponseModel = response.body();
                if (getVotersResponseModel.getStatusCode() == 200) {
                    voterList = getVotersResponseModel.getVoterList();


                    setupAadapter();
                    myDbHelper.saveOrUpdateDataInLocalDbFromApi(voterList);

                }
            }

            @Override
            public void onFailure(Call<GetVotersResponseModel> call, Throwable t) {
                progressBar.dismiss();
            }
        });

    }


    private GetVotersRequestModel createGetVotersRequestModel()
    {
        GetVotersRequestModel getVotersRequestModel = new GetVotersRequestModel();

        return getVotersRequestModel;
    }


    private void setupAadapter() {
        UnsurveyedDataAdapter unsurveyedDataAdapter = new UnsurveyedDataAdapter(context, voterList);
        recyclerview_all.setAdapter(unsurveyedDataAdapter);
    }

}
