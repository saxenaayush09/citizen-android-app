package com.tact.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tact.database.MyDbHelper;
import com.tact.kumbhca.R;
import com.tact.model.VoterData;
import com.tact.network.ApiClient;
import com.tact.network.ApiClientInterface;
import com.trend.progress.ProgressDialog;

import java.util.ArrayList;

public class ServeyedFragment extends Fragment {
    View view;
    Context context;
    MyDbHelper myDbHelper;


    private ApiClientInterface apiClientInterface;
    private RecyclerView recyclerview_all;
    ProgressDialog progressBar;
    ArrayList<VoterData> syncedvoterData;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_unserveyed, container, false);
        initWidgets();
        initWidgetListener();

        return view;
    }

    private void initWidgetListener() {


    }

    private void initWidgets() {
        context = getActivity();
        context = getActivity();
        apiClientInterface = ApiClient.getApiClient().create(ApiClientInterface.class);
        recyclerview_all = view.findViewById(R.id.recyclerview_all);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerview_all.setLayoutManager(linearLayoutManager);
        progressBar = new ProgressDialog(context);
        progressBar.setBarColor(getResources().getColor(R.color.red));
        myDbHelper = new MyDbHelper(context);
        syncedvoterData = myDbHelper.getSyncedData();
        setupAadapter();


    }

    private void setupAadapter() {
        UnsurveyedDataAdapter unsurveyedDataAdapter = new UnsurveyedDataAdapter(context, syncedvoterData);
        recyclerview_all.setAdapter(unsurveyedDataAdapter);
    }


}
