package com.tact.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.tact.activity.LoginActivity;
import com.tact.database.MyDbHelper;
import com.tact.model.GetVotersResponseModel;
import com.tact.model.VoterData;
import com.tact.network.ApiClient;
import com.tact.network.ApiClientInterface;
import com.tact.utils.Constant;
import com.tact.utils.SharedPreference;
import com.tact.utils.Support;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SyncingService extends IntentService
{

    private MyDbHelper myDbHelper;
    ApiClientInterface apiClientInterface;
    SharedPreference sharedPreference;

    public SyncingService()
    {
        super("syncing");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent)
    {
        myDbHelper = new MyDbHelper(getApplicationContext());
        apiClientInterface = ApiClient.getApiClient().create(ApiClientInterface.class);
        sharedPreference = new SharedPreference(getApplicationContext());
        startSyncingProcess();
    }


    private void startSyncingProcess()
    {
        ArrayList<VoterData> unsyncedVoterList = myDbHelper.getUnsyncedData();
        for (int i = 0; i < unsyncedVoterList.size(); i++)
        {
            final VoterData voterData = unsyncedVoterList.get(i);
            voterData.setUserId(sharedPreference.getValueInt(Constant.KEY_USER_ID));

            apiClientInterface.syncVoters(voterData).enqueue(new Callback<GetVotersResponseModel>()
            {
                @Override
                public void onResponse(Call<GetVotersResponseModel> call, Response<GetVotersResponseModel> response)
                {
                    if (response.isSuccessful())
                    {
                        voterData.setIsSynced(1);
                        myDbHelper.updateVoterData(voterData);
                        Support.showToast(getApplicationContext() , "Sync success");
                    }
                    else
                    {
                        Support.showToast(getApplicationContext() , "Server error");
                    }
                }

                @Override
                public void onFailure(Call<GetVotersResponseModel> call, Throwable t)
                {
                    t.printStackTrace();
                }
            });

        }
    }
}
