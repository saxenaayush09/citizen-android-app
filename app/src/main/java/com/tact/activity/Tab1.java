package com.tact.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.tact.adapter.CompetitorsListAdapter;
import com.tact.adapter.DataTransferInterface;
import com.tact.kumbhca.R;

public class Tab1 extends AppCompatActivity implements DataTransferInterface{

    private ListView priCompetitor;

    String[] aSerialNumber = {"1 ", "2" , "3", "4 ", "5" ,"6" , "7" , "8" , "9" , "10" , "11" , "12" };
    String[] aName = {"आयुष सक्सेना  ", "दीपक गुप्ता  ", "शिवम् ज्योति " , "अनुराग मिश्र ", "रामदुलारी  देवी", "मुकेश बाबू " , "आयुष सक्सेना  ", "दीपक गुप्ता  ", "शिवम् ज्योति " , "अनुराग मिश्र ", "रामदुलारी  देवी", "मुकेश बाबू "};
    String[] aFatherName = {"अनिल सक्सेना  ", "केसरीलाल  गुप्ता ", "ज्योती प्रकाश " , "निखिल मिश्रा  ", "रामखिलावन   ", "रामप्रसाद  ", "अनिल सक्सेना  ", "केसरीलाल  गुप्ता ", "ज्योती प्रकाश " , "निखिल मिश्रा  ", "रामखिलावन   ", "रामप्रसाद  "};
    String[] aAge = {"25 ", "28" , "29", "30", "35" , "40" , "25 ", "28" , "29", "30", "35" , "40"};
    String[] aEducationName = {"१२ /२० लखनऊ ", "१३/३४  फैज़ाबाद ", "१४ /१७ गोरखपुर " , "१२/४५ गोंडा  ", "१२ /२० लखनऊ ", "१३/३४  फैज़ाबाद ", "१४ /१७ गोरखपुर " , "१२/४५ गोंडा  ", "१४ /१७ गोरखपुर " , "१२/४५ गोंडा  ", "१३/३४  फैज़ाबाद ", "१४ /१७ गोरखपुर " };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab1);

        priCompetitor = (ListView) findViewById(R.id.pricompetitorlist);
        setAdapter();

        //ayush
    }

    private void setAdapter()
    {
        CompetitorsListAdapter priAdapter = new CompetitorsListAdapter(Tab1.this , this);
        priCompetitor.setAdapter(priAdapter);

//        priCompetitor.setOnItemClickListener(new AdapterView.OnItemClickListener()
//        {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
//            {
//                Intent accountsIntent = new Intent(Tab1.this , DataCollection.class);
//
//                Log.d("Hi" , aSerialNumber[position]);
//                accountsIntent.putExtra(" SERIAL_NUMBER" , aSerialNumber[position]);
//                accountsIntent.putExtra(" NAME" , aName[position]);
//                accountsIntent.putExtra(" FATHER_NAME" , aFatherName[position]);
//                accountsIntent.putExtra(" AGE" , aAge[position]);
//                accountsIntent.putExtra(" EDUCATION_NAME" , aEducationName[position]);
//
//                startActivity(accountsIntent);
//            }
//        });

    }

    @Override
    public void setValues(String name)
    {

    }
}
