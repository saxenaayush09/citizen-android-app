//Simple Android TabHost and TabWidget Example
package com.tact.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.Toast;
import android.widget.Toolbar;
import com.tact.kumbhca.R;

public class Dashboard extends TabActivity
{
    TabHost tabHost;
    private long lastPress;
    private TabHost tabHostWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        initToolBar("Dashboard");

        // create the TabHost that will contain the Tabs
        TabHost tabHost = (TabHost) findViewById(android.R.id.tabhost);


        TabHost.TabSpec tab1 = tabHost.newTabSpec("First Tab");
        TabHost.TabSpec tab2 = tabHost.newTabSpec("Second Tab");

        // Set the Tab name and Activity
        // that will be opened when particular Tab will be selected
        tab1.setIndicator("एकत्रित डाटा ");
        tab1.setContent(new Intent(this, Tab1.class));

        tab2.setIndicator("अनएकत्रित डाटा ");
        tab2.setContent(new Intent(this, Tab2.class));


        /** Add the tabs  to the TabHost to display. */
        tabHost.addTab(tab1);
        tabHost.addTab(tab2);
    }

    private void initToolBar(String title)
    {
//        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
//        //setSupportActionBar(toolbar);
//        setTitle(title);
    }

    @Override
    public void onBackPressed()
    {
        long currentTime = System.currentTimeMillis();
        if (currentTime - lastPress > 5000)
        {
            Toast.makeText(getBaseContext(), "Press back again to logout", Toast.LENGTH_LONG).show();
            lastPress = currentTime;
        }
        else
            {
            super.onBackPressed();
        }
    }
}