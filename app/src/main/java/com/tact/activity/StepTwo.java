package com.tact.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.tact.kumbhca.R;
import com.tact.model.PollingStationModel;
import com.tact.model.PollingStationOneModel;
import com.tact.model.PollingStationTwoModel;
import com.tact.network.ApiClient;
import com.tact.network.ApiClientInterface;
import com.tact.utils.Constant;
import com.tact.utils.SharedPreference;
import com.tact.utils.Support;
import com.trend.progress.ProgressDialog;

import java.util.ArrayList;
import java.util.List;

import gr.escsoft.michaelprimez.searchablespinner.SearchableSpinner;
import gr.escsoft.michaelprimez.searchablespinner.interfaces.IStatusListener;
import gr.escsoft.michaelprimez.searchablespinner.interfaces.OnItemSelectedListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StepTwo extends AppCompatActivity
{
    ProgressDialog progressBar;
    ApiClientInterface apiClientInterface;
    private TextView tvPAreaName;
    private int user_id;
    private int ac_no;
    private String prefix;
    private SharedPreference sp;
    private TextView tvPickedUserAccount;

    private SearchableSpinner mSearchableSpinner3;
    private SimpleListAdapter mSimpleListAdapter;

    private List<PollingStationTwoModel> listPollingStation;

    private final ArrayList<String> mStrings = new ArrayList<>();
    private int PS_NO;
    private String assemblyName;

    public int pos = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_two);

        sp = new SharedPreference(this);
        apiClientInterface = ApiClient.getApiClient().create(ApiClientInterface.class);
        progressBar = new ProgressDialog(this);
        progressBar.setBarColor(getResources().getColor(Constant.DEFAULT_PROGRESS_COLOR));

        mSimpleListAdapter = new SimpleListAdapter(this, mStrings);

        initToolBar("Step: 2");
        init();
        getDataFromBundle();
        getPollingStation(user_id, ac_no, prefix);
    }

    private void getPollingStation(int user_id, int ac_no, String prefix)
    {
        progressBar.show();

        PollingStationModel psm = new PollingStationModel();
        psm.setUserId(user_id);
        psm.setAcNo(ac_no);
        psm.setPrefix(prefix);

        apiClientInterface.getPollingStation(psm).enqueue(new Callback<PollingStationOneModel>()
        {
            @Override
            public void onResponse(Call<PollingStationOneModel> call, Response<PollingStationOneModel> response)
            {
                progressBar.dismiss();

                if (response.isSuccessful())
                {
                    if (response.body().getStatusCode() == 200)
                    {
                        listPollingStation = response.body().getData();

                        for (int i =1 ; i < listPollingStation.size(); i++)
                        {
                            mStrings.add(listPollingStation.get(i).getPsName());
                        }
                    }
                    else if (response.body().getStatusCode() == 500)
                    {
                        Support.showAlertDialog(StepTwo.this, "Server Error");
                    }
                }
                else
                {
                    Support.showAlertDialog(StepTwo.this, "Server Error!");
                }
            }

            @Override
            public void onFailure(Call<PollingStationOneModel> call, Throwable t)
            {
                t.printStackTrace();
            }
        });
    }

    private void initToolBar(String title)
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setHomeButtonEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(title);
    }

    private void init()
    {
        tvPAreaName = (TextView) findViewById(R.id.tvPAreaName);
        tvPickedUserAccount = (TextView) findViewById(R.id.tvPickedUserAccount);


        mSearchableSpinner3 = (SearchableSpinner) findViewById(R.id.SearchableSpinner3);
        mSearchableSpinner3.setAdapter(mSimpleListAdapter);

        mSearchableSpinner3.setOnItemSelectedListener(mOnItemSelectedListener);
        mSearchableSpinner3.setStatusListener(new IStatusListener()
        {
            @Override
            public void spinnerIsOpening()
            {
                mSearchableSpinner3.hideEdit();
            }

            @Override
            public void spinnerIsClosing()
            {

            }
        });


        //Creating the instance of ArrayAdapter containing list of fruit names
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>
//                (this, android.R.layout.select_dialog_item, fruits);
//        //Getting the instance of AutoCompleteTextView
//        final AutoCompleteTextView actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
//        actv.setThreshold(0);//will start working from first character
//        actv.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
//
//        actv.setTextColor(Color.RED);
//
//        actv.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View view)
//            {
//                actv.showDropDown();
//            }
//        });

        Button appCompatButtonLogin = (Button) findViewById(R.id.appCompatButtonLogin);
        appCompatButtonLogin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (pos != 0)
                {
                    Intent intent = new Intent(StepTwo.this, NewDashboardScreen.class);
                    intent.putExtra("PsNo" , PS_NO);
                    intent.putExtra("AcNo" , ac_no);
                    intent.putExtra("Prefix" , prefix);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                }
                else
                {
                    Support.showAlertDialog(StepTwo.this , "Select polling station");
                }

            }
        });
    }



    private OnItemSelectedListener mOnItemSelectedListener = new OnItemSelectedListener()
    {
        @Override
        public void onItemSelected(View view, int position, long id)
        {
            try
            {
                Toast.makeText(StepTwo.this,  listPollingStation.get(position).getPsName()+"", Toast.LENGTH_SHORT).show();

                PS_NO = listPollingStation.get(position).getPsNo();


                pos = position;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public void onNothingSelected()
        {
            Toast.makeText(StepTwo.this, "Nothing Selected", Toast.LENGTH_SHORT).show();
        }
    };


    private void getDataFromBundle()
    {
        Bundle b = getIntent().getExtras();

        if (b != null)
        {
            user_id = b.getInt("USER_ID");
            ac_no = b.getInt("AC_NO");
            prefix = b.getString("Prefix");
            assemblyName = b.getString("ASSEMBLY_NAME");

            tvPAreaName.setText(b.getString("PAN"));
            tvPickedUserAccount.setText(assemblyName);
        }
    }
}
