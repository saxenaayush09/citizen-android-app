package com.tact.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.tact.adapter.ViewPagerAdapter;
import com.tact.kumbhca.R;

public class NewDashboardScreen extends AppCompatActivity {

    private Context context;

    private Toolbar toolbar;
    private TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter adapter;
    private int ps_no;
    private int ac_no;
    private String prefix;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_dashboard);

        initToolBar("Citizen Records");
        getDataFromBundle();
        initWidgets();
        initWidgetListener();
    }

    private void initToolBar(String title)
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(title);
    }

    private void getDataFromBundle()
    {
        Bundle b = getIntent().getExtras();

        if (b != null)
        {
            ps_no = b.getInt("PsNo");
            ac_no = b.getInt("AcNo");
            prefix = b.getString("Prefix");
        }
    }

    private void initWidgetListener()
    {

    }

    private void initWidgets()
    {
        context = NewDashboardScreen.this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        adapter = new ViewPagerAdapter(getSupportFragmentManager() , ps_no, ac_no, prefix);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(1);

        tabLayout.setupWithViewPager(viewPager);


     /*   for (int i = 0; i < tabLayout.getTabCount(); i++) {
            //noinspection ConstantConditions
            TextView tv = (TextView) LayoutInflater.from(context).inflate(R.layout.layout_custom_tab, null);

            tabLayout.getTabAt(i).setCustomView(tv);
        }*/


    }
}
