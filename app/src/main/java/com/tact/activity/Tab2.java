package com.tact.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.tact.adapter.CompetitorsListAdapter2;
import com.tact.kumbhca.R;

public class Tab2 extends AppCompatActivity
{

    private ListView priCompetitor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab2);

        priCompetitor = (ListView) findViewById(R.id.pricompetitorlist);
        setAdapter();
    }

    private void setAdapter()
    {
        CompetitorsListAdapter2 priAdapter = new CompetitorsListAdapter2(Tab2.this);
        priCompetitor.setAdapter(priAdapter);
    }
}
