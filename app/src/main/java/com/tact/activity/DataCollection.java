package com.tact.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.tact.adapter.DataTransferInterface;
import com.tact.kumbhca.R;
import com.tact.utils.SharedPreference;

public class DataCollection extends AppCompatActivity implements DataTransferInterface{

    EditText etSerialNumber;
    EditText etName;
    EditText etFatherName;
    EditText etAge;
    EditText etEducationName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_collection);

        initToolBar("सिटीजन रिकॉर्ड ");

        init();


    }

    private void initToolBar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
        setTitle(title);
    }

    private void init() {
        etSerialNumber = (EditText) findViewById(R.id.tvSerialNumber);
        etName = (EditText) findViewById(R.id.tvName);
        etFatherName = (EditText) findViewById(R.id.tvFatherName);
        etAge = (EditText) findViewById(R.id.tvAge);
        etEducationName = (EditText) findViewById(R.id.tvEducationName);

        AppCompatButton appCompatButtonLogin = (AppCompatButton) findViewById(R.id.appCompatButtonLogin);
        appCompatButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Toast.makeText(DataCollection.this,
                        "डाटा सफलता पूर्वक सुरक्षित कर लिया गया है धन्यवाद !", Toast.LENGTH_SHORT).show();
            }
        });




        SharedPreference sp = new SharedPreference(DataCollection.this);
        Log.d("Hi Deepak" , sp.getValueString("SERIAL_NUMBER"));

        etSerialNumber.setText(sp.getValueString("SERIAL_NUMBER"));
        etName.setText(sp.getValueString("NAME"));
        etFatherName.setText(sp.getValueString("FATHER_NAME"));
        etAge.setText(sp.getValueString("AGE"));
        etEducationName.setText(sp.getValueString("EDUCATION_NAME"));
    }

    @Override
    public void setValues(String name)
    {
     Log.d("Name" , name);
    }
}
