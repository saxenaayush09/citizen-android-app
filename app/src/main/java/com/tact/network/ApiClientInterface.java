package com.tact.network;

import com.tact.model.GetVotersRequestModel;
import com.tact.model.GetVotersResponseModel;
import com.tact.model.PollingStationModel;
import com.tact.model.PollingStationOneModel;
import com.tact.model.SignInOneModel;
import com.tact.model.SignInSendModel;
import com.tact.model.VoterData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiClientInterface {

//    @Headers("Content-type: application/json")
//    @GET("Register/GenrateOTP")
//    public Call<LoginMobileNoModel> generateOtp(@Query("MobileNo") String mobileNumber);
//
//
//    @Headers("Content-type: application/json")
//    @GET("Register/VerifyOTP")
//    public Call<LoginOtpModel> verifyOtp(@Query("MobileNo") String mobileNumber, @Query("OTP") String otp);
//
//
//    @POST("Register/RegisterUser")
//    Call<SignUpActivityModel> signUp(@Body SignUpModelRetrofit retrofit);
//

    @POST("api/CA/SignIn")
    Call<SignInOneModel> signIn(@Body SignInSendModel retrofit);


    @POST("api/CA/GetPolingStation")
    Call<PollingStationOneModel> getPollingStation(@Body PollingStationModel retrofit);

    @POST("api/CA/GetVoters")
    Call<GetVotersResponseModel> getVoters(@Body GetVotersRequestModel getVotersRequestModel);


    @POST("api/CA/SyncVoter")
    Call<GetVotersResponseModel> syncVoters(@Body VoterData voterData);

}