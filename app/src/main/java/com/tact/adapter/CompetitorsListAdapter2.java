package com.tact.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tact.kumbhca.R;
import com.tact.activity.DataCollection;

/*
This class shows the list of all the competitors in PRI Result Search and is used with the competitors fragment
 */
public class CompetitorsListAdapter2 extends BaseAdapter
{

    public LayoutInflater inflater;
    Context con;

    String[] aSerialNumber = {"1 ", "2" , "3", "4 ", "5" ,"6" , "7" , "8" , "9" , "10" , "11" , "12" };
    String[] aName = { "रामदुलारी  देवी", "मुकेश बाबू " , "आयुष सक्सेना  ","आयुष सक्सेना  ", "दीपक गुप्ता  ", "शिवम् ज्योति " , "अनुराग मिश्र ", "दीपक गुप्ता  ", "शिवम् ज्योति " , "अनुराग मिश्र ", "रामदुलारी  देवी", "मुकेश बाबू "};
    String[] aFatherName = { "रामखिलावन   ", "रामप्रसाद  ", "अनिल सक्सेना  ", "केसरीलाल  गुप्ता ",  "अनिल सक्सेना  ", "केसरीलाल  गुप्ता ", "ज्योती प्रकाश " , "निखिल मिश्रा  ", "ज्योती प्रकाश " , "निखिल मिश्रा  ", "रामखिलावन   ", "रामप्रसाद  "};
    String[] aAge = {"25 ", "28" , "29", "30", "35" , "40" , "25 ", "28" , "29", "30", "35" , "40"};
    String[] aEducationName = { "१२ /२० लखनऊ ", "१३/३४  फैज़ाबाद ", "१४ /१७ गोरखपुर " , "१२/४५ गोंडा  " ,"१२ /२० लखनऊ ", "१३/३४  फैज़ाबाद ", "१४ /१७ गोरखपुर " , "१२/४५ गोंडा  ", "१४ /१७ गोरखपुर " , "१२/४५ गोंडा  ", "१३/३४  फैज़ाबाद ", "१४ /१७ गोरखपुर " };

    public CompetitorsListAdapter2(Context con)
    {
        this.con = con;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent)
    {
        if (view == null)
        {
            view = inflater.inflate(R.layout.non_collected_adapter, null);
        }


        TextView tvSerialNumber = (TextView) view.findViewById(R.id.tvSerialNumber);
        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        TextView tvFatherName = (TextView) view.findViewById(R.id.tvFatherName);
        TextView tvAge = (TextView) view.findViewById(R.id.tvAge);
        TextView tvEducationName = (TextView) view.findViewById(R.id.tvEducationName);

        tvSerialNumber.setText(aSerialNumber[position]);
        tvName.setText(aName[position]);
        tvFatherName.setText(aFatherName[position]);
        tvAge.setText(aAge[position]);
        tvEducationName.setText(aEducationName[position]);

        CardView cardViewCitizen = (CardView) view.findViewById(R.id.cardViewCitizen);
        cardViewCitizen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent accountsIntent = new Intent(con, DataCollection.class);

                accountsIntent.putExtra(" tvSerialNumber" , aSerialNumber[position]);
                accountsIntent.putExtra(" tvName" , aName[position]);
                accountsIntent.putExtra(" tvFatherName" , aFatherName[position]);
                accountsIntent.putExtra(" tvAge" , aAge[position]);
                accountsIntent.putExtra(" tvEducationName" , aEducationName[position]);

                con.startActivity(accountsIntent);
            }
        });

        return view;
    }

    @Override
    public int getCount()
    {
        return 12;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

}
